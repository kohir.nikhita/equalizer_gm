package com.example.nikhita.soundequilizers

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class SoundEqulizersAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {

            0 -> return EqulizerFragment()
            1 -> return FadeFragment()

        }

        return return SoundFragment()

    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Equilizer"
            1 -> "Sound Fade/Balance"
            else -> {
                return "Sound Mode"
            }
        }
    }
}